package com.sender.locally1;

import java.util.Date;

public class ScheduledMsg {
    String message;
    Date delay;
    String userName;
    String channelName;
    String token;

    public ScheduledMsg(String message, Date delay, String userName, String channelName, String token) {
        this.message = message;
        this.delay = delay;
        this.userName = userName;
        this.channelName = channelName;
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDelay() {
        return delay;
    }

    public void setDelay(Date delay) {
        this.delay = delay;
    }


}
