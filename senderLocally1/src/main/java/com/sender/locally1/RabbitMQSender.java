package com.sender.locally1;

import com.rabbitmq.client.AMQP;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
class MessageServiceImpl {
    private static final Logger logger = LoggerFactory.getLogger(MessageServiceImpl.class);
    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sendMsg(String exchangeName, String queueName,
                        String msg, Integer delay) {
        logger.info(">>>>send message");
        logger.info(msg);

        rabbitTemplate.convertAndSend(exchangeName, queueName, msg, message -> {
            //header x-delay must be added
            message.getMessageProperties().setHeader("x-delay", delay);
            return message;
        });
    }
}

@Service
public class RabbitMQSender {
    @Autowired
    private AmqpTemplate rabbitTemplate;

    @Value("${sample.rabbitmq.exchange}")
    private String exchange;

    @Value("${sample.rabbitmq.routingkey}")
    private String routingkey;

    @Scheduled
    public void send(String message) {
        String CustomMessage = "This is a message from sender " + message;

        rabbitTemplate.convertAndSend(exchange, routingkey, CustomMessage);
        System.out.println("Send msg to consumer= " + CustomMessage + " ");

        /*MessageProperties properties = new MessageProperties();
        properties.setHeader("x-delay", 15000);
        rabbitTemplate.convertAndSend(exchange, routingkey,
                MessageBuilder.withBody(message.getBytes()).andProperties(properties).build());*/

    }
}