package com.sender.locally1;

import java.io.IOException;
import java.util.HashMap;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.RequestEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rabbitmq")// , method = RequestMethod.POST value =
public class Controller {

    @Autowired
    MessageServiceImpl messageService;
    //RabbitMQSender rabbitMQSender;


    @Value("${sample.rabbitmq.queue}") // +++
    private String queueName;

    @Value("${sample.rabbitmq.exchange}") // +++
    private String exchangeName;

    /* public String producer(@RequestParam(value = "message") String message,
                            @RequestParam(value = "delay") int myDelay)*/
    @PostMapping("/producer") //value =
    public String producer(@RequestBody ScheduledMsg  scheduledMsg)
            throws IOException {


        System.out.println(scheduledMsg.delay.getTime() + '-' + System.currentTimeMillis());
        System.out.println("long type: " + (scheduledMsg.delay.getTime() - System.currentTimeMillis()));
        System.out.println("int type: " + (int) (scheduledMsg.delay.getTime() - System.currentTimeMillis()));
        int delay = (int) (scheduledMsg.delay.getTime() - System.currentTimeMillis());
        if (delay < 0) { return null; }

        System.out.println("this is controller : " + scheduledMsg.message);
        System.out.println("this is myDelay : " + scheduledMsg.delay);
        System.out.println("this is myDelay : " + scheduledMsg.userName);
        System.out.println("this is myDelay : " + scheduledMsg.channelName);
        System.out.println("this is queueName : " + queueName);
        System.out.println("this is exchange : " + exchangeName);

        //rabbitMQSender.send(message);
        ObjectMapper mapper = new ObjectMapper();
        String scheduledMsgS = mapper.writeValueAsString(scheduledMsg);

        messageService.sendMsg(exchangeName, queueName, scheduledMsgS, delay);

        System.out.println("this is scheduledMsgS: " + scheduledMsgS);

        //return "Message sent to the RabbitMQ Successfully";
        return scheduledMsgS;
    }
}

