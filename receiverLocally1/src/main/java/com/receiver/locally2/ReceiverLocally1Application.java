package com.receiver.locally2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.bind.annotation.RequestMapping;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
public class ReceiverLocally1Application {

	public static void main(String[] args) {
		SpringApplication.run(ReceiverLocally1Application.class, args);
	}

}
