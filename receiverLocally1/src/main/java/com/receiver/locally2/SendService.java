package com.receiver.locally2;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("message-service")
public interface SendService {
    @PostMapping("/message")
    public String sendScheduledMessage(@RequestBody String body);
}
