package com.receiver.locally2;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

/*@RestController
class ConsumeWebService {
    @Autowired
    RestTemplate restTemplate;

    @RequestMapping(value = "/template/scheduledMsg", method = RequestMethod.POST)
    public String createProducts(@RequestBody ScheduledMsg scheduledMsg) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<ScheduledMsg> entity = new HttpEntity<ScheduledMsg>(scheduledMsg, headers);

        return restTemplate.exchange(
                "http://localhost:5051/scheduledMsg", HttpMethod.POST, entity, String.class).getBody();
    }
}*/

@Component
class MessageReceiver {
    private static final Logger logger = LoggerFactory.getLogger(MessageReceiver.class);

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    SendService sendService;

    @RabbitListener(queues = "${sample.rabbitmq.queue}")
    public void receive(String scheduledMsgS) throws JsonProcessingException {
        logger.info("Message received:{}", scheduledMsgS);

        System.out.println("Recieved Message From RabbitMQ: " + scheduledMsgS);

        ObjectMapper mapper = new ObjectMapper();
        ScheduledMsg scheduledMsg = mapper.readValue(scheduledMsgS, ScheduledMsg.class);

        System.out.println("Recieved Message From RabbitMQ: " + scheduledMsg.toString());

        String body=
                "{\"content\":\""+scheduledMsg.message+"\","+
                "{\"sender\":\""+scheduledMsg.userName+"\","+
                "{\"channel\":\""+scheduledMsg.channelName +"\"}";

        //sendService.sendScheduledMessage(body);
        /*HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<ScheduledMsg> entity = new HttpEntity<ScheduledMsg>(scheduledMsg, headers);

        ResponseEntity rt = restTemplate.exchange(
                "http://192.168.43.6:5051/api/messages/async", HttpMethod.POST, entity, String.class);

        if (rt.getStatusCode() != HttpStatus.ACCEPTED) {
            System.out.println("not an Accepted request buddy!!!");

        }
        System.out.println("Send Message to asyncMessage with body: " + rt.getBody().toString());
*/
        HttpHeaders headers = new HttpHeaders();
        headers.set("authrization","Bearer "+scheduledMsg.token);
        HttpEntity<ScheduledMsg> entity = new HttpEntity<ScheduledMsg>(scheduledMsg, headers);

        ResponseEntity res=restTemplate.exchange("http://192.168.43.203:4444/message",
                HttpMethod.POST,entity,Object.class);
    }

}

/*@Component
public class RabbitMQ {

    @RabbitListener(queues = "${sample.rabbitmq.queue}")
    public void recievedMessage(String incomingMessage) {
        System.out.println("Recieved Message From RabbitMQ: " + incomingMessage);

    }
}*/